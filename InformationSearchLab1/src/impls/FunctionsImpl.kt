package impls

import interfaces.Functions

/**
 * Класс, реализующий интерфейс Functions
 */
class FunctionsImpl : Functions {
    override fun calculate(a: Int, b: Int) = a + b
    override fun substringCounter(s: String, sub: String): Int {
        return s.count { it.toString() == sub }
    }

    override fun splitterSorter(s: String, sub: String): List<String> {
        val list = s.split(sub)
        return list.toList()
    }

    override fun uniqueCounter(s: String, sub: String): Map<String, Int> {
        val a = s.split(sub)
        val dict = mutableMapOf<String, Int>()

        for (item in a) {

            dict[item] = (dict[item] ?: 0) + 1

        }

        return dict
    }

    override fun isPalindrome(s: String): Boolean {
        if (s == "") {
            return false
        }
    }

    override fun invert(s: String): String {
        val reversedString = s.reversed()
        return s.equals(reversedString, ignoreCase = true)
    }
}
